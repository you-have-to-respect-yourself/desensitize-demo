package com.example.demo.desensitization.annotation;

import com.example.demo.desensitization.enums.DesensitizationFiledEnum;

/**
 * @author EDY
 */
public @interface DesensitizationFiled {
    /**
     * 字段名
     * @return
     */
    String value();

    /**
     * 字段前保留数
     * @return
     */
    int preLength() default -1;

    /**
     * 数据后保留位数
     * @return
     */
    int endLength() default -1;;

    /**
     * 脱敏字段长度
     * @return
de   */
    int columnLength() default -1;;
    DesensitizationFiledEnum deFiledEnum() default DesensitizationFiledEnum.NULL;

}
