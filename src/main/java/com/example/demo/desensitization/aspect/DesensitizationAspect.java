package com.example.demo.desensitization.aspect;

import com.example.demo.desensitization.annotation.Desensitization;
import com.example.demo.desensitization.annotation.DesensitizationFiled;
import com.example.demo.desensitization.util.DesensitizedUtil;
import com.example.demo.response.BaseResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Aspect
@Slf4j
@Component
public class DesensitizationAspect {

    @Pointcut("@annotation(com.example.demo.desensitization.annotation.Desensitization)")
    public void serviceAspect() {

    }

    @Around("serviceAspect()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Object isDesensitized = request.getParameter("desensitized");
        MethodSignature methodSignature = ((MethodSignature) pjp.getSignature());
        Desensitization desensitization = methodSignature.getMethod().getAnnotation(Desensitization.class);
        Object result = pjp.proceed();
        if (Objects.isNull(isDesensitized) || String.valueOf(Boolean.FALSE.booleanValue()).equals(String.valueOf(isDesensitized))) {
            return result;
        }
        return deFiledValue(desensitization.value(), result);
    }

    private static Object deFiledValue(DesensitizationFiled[] filed, Object result) {
        Stream<DesensitizationFiled> st = Arrays.stream(filed);
        try {
            st.forEach(v -> {

                if (result instanceof BaseResponse) {
                    Object data = ((BaseResponse<?>) result).getData();
                    if (Objects.isNull(data)) {
                        return;
                    }
                    deFiledValue2(v, data);
                } else {
                    deFiledValue2(v, result);
                }
            });
            return result;
        } catch (Exception e) {
            log.error("脱敏异常：{},result:{},anon:{}", e, result, filed);
            return result;
        }

    }

    private static Object deFiledValue2(DesensitizationFiled filed, Object result) {
        if (result instanceof List) {
            ((List<?>) result).stream().forEach(vv ->
            {
                if (Objects.isNull(vv)) {
                    return;
                }
                try {
                    DesensitizedUtil.desensitize(filed, vv);
                } catch (Exception e) {
                    log.error("脱敏异常：{},value:{}", e, filed);
                }
            });

        } else {
            DesensitizedUtil.desensitize(filed, result);
        }
        return result;
    }


}
