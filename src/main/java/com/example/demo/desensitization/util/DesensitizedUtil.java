package com.example.demo.desensitization.util;

import com.example.demo.desensitization.annotation.DesensitizationFiled;
import com.example.demo.desensitization.enums.DesensitizationFiledEnum;
import com.example.demo.response.BaseResponse;
import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;


/**
 * @description: 脱敏工具类
 * @author: jiaxuan.xu
 * @createDate: 2021/10/25
 * @version: 1.0
 */
public class DesensitizedUtil {

    private static final Logger log = LoggerFactory.getLogger(DesensitizedUtil.class);
    /**
     * 脱敏符号
     */
    private static final String SYMBOL = "*";
    private static final String MOBILE_REGEX = "[(]0\\d{2,3}-\\d{7,8}|\\(?0\\d{2,3}[)-]?\\d{7,8}|\\(?0\\d{2,3}[)-]*\\d{7,8}";

    /**
     * 对字符传进行脱敏
     *
     * @param preLength    数据前保留位数
     * @param endLength    数据前保留位数
     * @param columnLength 字符串长度 当columnLength不为空 需脱敏长度为columnLength 才进行脱敏处理
     * @param value
     * @return
     */
    public static String desensitizeString(Integer preLength, Integer endLength, Integer columnLength, String value) {
        if (StringUtils.isEmpty(value)) {
            return value;
        }
        if (columnLength != -1 && columnLength != null && columnLength != 0 && value.length() != columnLength) {
            return value;
        }
        String regex = "(?<=\\S{" + preLength + "})\\S(?=\\S{" + endLength + "})";
        return value.replaceAll(regex, SYMBOL);
    }

    /**
     *
     * @param desensitizationFiledEnum 枚具值
     * @param value
     * @return
     */
    public static String desensitizeEnum(DesensitizationFiledEnum desensitizationFiledEnum, String value) {
        if (StringUtils.isEmpty(value)) {
            return value;
        }
        if (desensitizationFiledEnum.getColumnLength() != -1 && desensitizationFiledEnum.getColumnLength() != null && desensitizationFiledEnum.getColumnLength() != 0 && value.length() != desensitizationFiledEnum.getColumnLength()) {
            return value;
        }
        String regex = "(?<=\\S{" + desensitizationFiledEnum.getPreLength() + "})\\S(?=\\S{" + desensitizationFiledEnum.getEndLength() + "})";
        return value.replaceAll(regex, SYMBOL);
    }


    /**
     * 对字符传进行脱敏
     *
     * @param preLength    数据前保留位数
     * @param endLength    数据前保留位数
     * @param columnLength 字符串长度 当columnLength不为空 需脱敏长度为columnLength 才进行脱敏处理
     * @param value
     * @return
     */
    public static List<String> desensitizeStringList(Integer preLength, Integer endLength, Integer columnLength, List<String> value) {
        List<String> result = new ArrayList<>();
        if (CollectionUtils.isEmpty(value)) {
            return new ArrayList();
        }
        value.stream().forEach(v ->
                result.add(desensitizeString(preLength, endLength, columnLength, String.valueOf(v)))
        );
        return result;
    }

    /**
     * 对字符传进行脱敏
     *
     * @param preLength    数据前保留位数
     * @param endLength    数据前保留位数
     * @param columnLength 字符串长度 当columnLength不为空 需脱敏长度为columnLength 才进行脱敏处理
     * @param value
     * @return
     */
    public static <T> List<T> desensitizeListEntity(Integer preLength, Integer endLength, Integer columnLength, List<T> value, String propertyName) {
        if (CollectionUtils.isEmpty(value)) {
            return new ArrayList();
        }
        value.stream().forEach(v ->
        {
            if (Objects.isNull(v)) {
                return;
            }
            try {
                PropertyUtils.setProperty(v, propertyName, desensitizeString(preLength, endLength, columnLength, String.valueOf(PropertyUtils.getProperty(v, propertyName))));
            } catch (Exception e) {
                log.error("脱敏异常：{},value:{}", e, v);
            }
        });
        return value;
    }

    /**
     * 对字符传进行脱敏
     *
     * @param preLength 数据前保留位数
     * @param endLength 数据前保留位数
     * @param value
     * @return
     */
    public static <T> List<T> desensitizeListEntity(Integer preLength, Integer endLength, List<T> value, String... propertyNames) {
        if (CollectionUtils.isEmpty(value)) {
            return new ArrayList();
        }
        value.stream().forEach(v ->
        {
            for (String propertyName : propertyNames) {
                try {
                    Object data = PropertyUtils.getProperty(v, propertyName);
                    if (Objects.isNull(data)) {
                        return;
                    }
                    String dataStr = String.valueOf(data);
                    PropertyUtils.setProperty(v, propertyName, desensitizeString(preLength, endLength, dataStr.length(), dataStr));
                } catch (Exception e) {
                    log.error("脱敏异常：{},value:{}", e, v);
                }
            }
        });
        return value;
    }

    /**
     * 11位电话号脱敏
     *
     * @param value
     * @return
     */
    public static String desensitizePhone(String value) {
        return desensitizeString(3, 3, 11, value);
    }

    /**
     * 11位电话号脱敏
     *
     * @param value
     * @return
     */
    public static <T> List<T> desensitizePhoneList(List<T> value, String propertyName) {
        return desensitizeListEntity(3, 3, 11, value, propertyName);
    }

    /**
     * 座机号脱敏
     *
     * @param value
     * @return
     */
    public static String desensitizeMobile(String value) {
        if (Pattern.matches(MOBILE_REGEX, value)) {
            return desensitizeString(5, 4, value.length(), value);
        }
        return value;
    }

    /**
     * 座机号脱敏
     *
     * @param value
     * @return
     */
    public static <T> List<T> desensitizeMobileList(List<T> value, String propertyName) {
        value.stream().forEach(v -> {
            try {
                Object data = PropertyUtils.getProperty(v, propertyName);
                if (Objects.isNull(data)) {
                    return;
                }
                PropertyUtils.setProperty(v, propertyName, desensitizeMobile(String.valueOf(data)));
            } catch (Exception e) {
                log.error("脱敏异常：{}", e);
            }
        });

        return value;
    }

    /**
     * 身份证号脱敏
     *
     * @param value
     * @return
     */
    public static String desensitizeIdCard(String value) {
        return desensitizeString(6, 4, 18, value);
    }

    /**
     * 身份证脱敏
     *
     * @param value
     * @return
     */
    public static <T> List<T> desensitizeIdCardList(List<T> value, String propertyName) {
        return desensitizeListEntity(6, 4, 18, value, propertyName);
    }

    /**
     * 护照脱敏
     *
     * @param value
     * @return
     */
    public static String desensitizeIdPassport(String value) {
        return desensitizeString(2, 3, value.length(), value);
    }

    /**
     * 护照脱敏
     *
     * @param value
     * @return
     */
    public static <T> List<T> desensitizeIdPassportList(List<T> value, String propertyName) {
        return desensitizeListEntity(2, 3, value, propertyName);
    }

    /**
     * 军官证、士兵证脱敏
     *
     * @param value
     * @return
     */
    public static String desensitizeSoldier(String value) {
        return desensitizeString(3, 2, value.length(), value);
    }

    /**
     * 军官证、士兵证脱敏
     *
     * @param value
     * @return
     */
    public static <T> List<T> desensitizeSoldierList(List<T> value, String propertyName) {
        return desensitizeListEntity(3, 2, value, propertyName);
    }

    /**
     * 警官证脱敏
     *
     * @param value
     * @return
     */
    public static String desensitizePolice(String value) {
        return desensitizeString(2, 2, value.length(), value);
    }

    /**
     * 警官证脱敏
     *
     * @param value
     * @return
     */
    public static <T> List<T> desensitizePoliceList(List<T> value, String propertyName) {
        return desensitizeListEntity(2, 2, value, propertyName);
    }


    public static Object desensitize(String propertyName, DesensitizationFiledEnum fileEnum, Object result) {
        return desensitize(propertyName,fileEnum,null,null,null,result);
    }
    public static Object desensitize(String propertyName, DesensitizationFiledEnum fileEnum, Integer preLengthRequest, Integer endLengthRequest, Integer columnLengthRequest, Object result) {
        int preLength = -1;
        int endLength = -1;
        int columnLength = -1;
        try {
            Object valueObject= PropertyUtils.getProperty(result, propertyName);
            if(Objects.isNull(valueObject)){
                return result;
            }
            String value = String.valueOf(valueObject);

            switch (fileEnum) {
                case ADDRESS:
                    preLength = value.indexOf("区");
                    if (preLength == -1) {
                        preLength = value.indexOf("县");
                    }
                    if (preLength == -1) {
                        break;
                    }
                    preLength = preLength + 1;
                    break;
                case EMAIL:
                    preLength = 0;
                    int index = value.indexOf("@");
                    if (index == -1) {
                        return result;
                    }
                    endLength = value.length() - index;
                    break;
                case NICK_NAME:
                    int len = value.length();
                    if (len >= 3) {
                        preLength = 1;
                        endLength = 1;
                    } else if (len == 2) {
                        preLength = 1;
                        endLength = 0;
                    } else {
                        preLength = 0;
                        endLength = 0;
                    }
                    break;

                case NULL:
                    if (Objects.nonNull(preLengthRequest) && Objects.nonNull(endLengthRequest) && Objects.nonNull(columnLengthRequest)) {
                        preLength = preLengthRequest;
                        endLength = endLengthRequest;
                        columnLength = columnLengthRequest;
                    } else {
                        log.warn("脱敏参数异常，脱敏终止：{} enumsValue is null,preLength：{}, endLength：{}, columnLength:{}", preLengthRequest, endLengthRequest, columnLengthRequest);
                        return result;
                    }

                    break;
                default:
                    preLength = fileEnum.getPreLength();
                    endLength = fileEnum.getEndLength();
                    columnLength = fileEnum.getColumnLength();
                    break;
            }
            if (preLength == -1 || endLength == -1) {
                return result;
            }
            PropertyUtils.setProperty(result, propertyName, desensitizeString(preLength, endLength, columnLength, value));
        } catch (Exception e) {
            log.error("脱敏异常：{},result:{},preLength：{}, endLength：{}, columnLength:{},enum:{}", e, result, preLength, endLength, columnLength, fileEnum);
        }
        return result;
    }

    public static Object desensitize(DesensitizationFiled filed, Object result) {
        return desensitize(filed.value(),filed.deFiledEnum(),filed.preLength(),filed.endLength(),filed.columnLength(),result);
    }

}
