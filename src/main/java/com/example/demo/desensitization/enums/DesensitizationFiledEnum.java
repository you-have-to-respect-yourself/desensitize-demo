package com.example.demo.desensitization.enums;

import lombok.Getter;

@Getter
public enum DesensitizationFiledEnum {

    NULL(),
    /**
     * 姓名，脱敏姓氏
     */
    NAME(0,1,-1),
    /**
     * 姓名，保留姓氏
     */
    SURNAME(1,0,-1),
    /**
     * 手机号
     */
    PHONE(3,4,-1),
    /**
     * 身份证、社保卡、驾驶证、居住证等18位号码
     */
    CARD_NO(0,4,18),
    /**
     * 军官证,士兵证
     */
    SOLDIER(3,0,-1),
    /**
     * 护照
     */
    ID_PASSPORT(3,0,-1),
    /**
     * 地址 隐藏区/县以下部分的地址
     */
    ADDRESS(),
    /**
     * 邮箱 隐藏@之前的内容
     */
    EMAIL(),
    /**
     * VIN 展示前3后3
     */
    VIN(3,3,-1),
    /**
     * 车牌号 展示前2后3
     */
    LICENSE(2,3,-1),
    /**
     * 用户昵称
     * 三个字及以上展示前1后1
     * 一个字的昵称全部脱敏
     * 两个字的自称掩盖后面一个字符
     */
    NICK_NAME(),

    /**
     * 发动机号   展示前1后2
     */
    ENGINE_NO(1,2,-1),
    /**
     * 精准定位信息GPS	经纬度后的数字全部隐藏
     */
    LOCATION(0,0,-1),
    /**
     * 用户ID	保留前1后1
     */
    USER_ID(1,1,-1),
    /**
     * 银行卡号	保留前1后1
     */
    BANK_CARD_NO(1,1,-1),
    /**
     * 密码/口令	隐藏全部
     */
    PWD(0,0,-1);




    private DesensitizationFiledEnum(){}
    private  DesensitizationFiledEnum(Integer preLength,Integer endLength,Integer columnLength){
        this.preLength = preLength;
        this.columnLength=columnLength;
        this.endLength=endLength;
    }

    /**
     * 字段前保留数
     * @return
     */
    Integer preLength ;

    /**
     * 数据后保留位数
     * @return
     */
    Integer endLength ;

    /**
     * 脱敏字段长度
     * @return
     */
    Integer columnLength ;




}
