package com.example.demo.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author EDY
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Test {


    private String text;
    private String test;
    private User user;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class User {
        private String phone;
        private String address;
    }
}
