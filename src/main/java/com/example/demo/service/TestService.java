package com.example.demo.service;

import com.example.demo.desensitization.annotation.Desensitization;
import com.example.demo.desensitization.annotation.DesensitizationFiled;
import com.example.demo.desensitization.enums.DesensitizationFiledEnum;
import com.example.demo.desensitization.util.DesensitizedUtil;
import com.example.demo.response.BaseResponse;
import com.example.demo.vo.Test;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class TestService {


//    @Desensitization({@DesensitizationFiled(value = "test",deFiledEnum = DesensitizationFiledEnum.NAME)})
    public List<Test> getList (Boolean desensitized){
        List<Test> list=new ArrayList();
        Test.User user = new Test.User();
        user.setPhone("1251541567");
        user.setAddress("ashasglsamgsdsg");
        Test t = Test.builder()
                .test("12324151251")
                .text("456")
                .user(user)
                .build();
        list.add(t);
//        for (Test o : list) {
//            String s = DesensitizedUtil.desensitizeString(1, 1, -1, o.getTest());
//            o.setTest(s);
//        }

        return list;
    }
    @Desensitization({@DesensitizationFiled(value = "test",preLength = 1,endLength = 1),
            @DesensitizationFiled(value = "text",preLength = 0,endLength = 1)})
    public Test get (Boolean desensitized){
        Test t=new Test();
        t.setTest("123");
        t.setText("456");
        return t;
    }

    @Desensitization({@DesensitizationFiled(value = "test",preLength = 1,endLength = 1),
            @DesensitizationFiled(value = "text",preLength = 0,endLength = 1)})
    public List<Test> getList1 (Boolean desensitized){

        return getList(desensitized);
    }

//    @Desensitization({@DesensitizationFiled(value = "test",deFiledEnum = DesensitizationFiledEnum.NAME)})
    public BaseResponse<List<Test>> getList2 (Boolean desensitized){
        Test test = get(desensitized);
        return new BaseResponse(test);
    }

    @Desensitization({@DesensitizationFiled(value = "test",deFiledEnum = DesensitizationFiledEnum.NAME), @DesensitizationFiled(value = "user.phone",deFiledEnum = DesensitizationFiledEnum.PHONE) })
    public BaseResponse<List<Test>> getList3 (Boolean desensitized){

        return new BaseResponse(getList(desensitized));
    }
}
