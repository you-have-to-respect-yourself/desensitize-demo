package com.example.demo.controller;

import com.example.demo.response.BaseResponse;
import com.example.demo.service.TestService;
import com.example.demo.vo.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TestController {
    Logger logger = LoggerFactory.getLogger(TestController.class);

    @Autowired
    TestService testService;
    @GetMapping("/test")
    public String test() {
        logger.info("test");
        return "ok";
    }

    @GetMapping("")
    public String home() {
        logger.info("home");
        return "ok";
    }

    @GetMapping("/test1")
    public Object test1(@RequestParam Boolean desensitized) {
        return testService.getList(desensitized);
    }

    @GetMapping("/test2")
    public Object test2(@RequestParam Boolean desensitized) {
        return testService.get(desensitized);
    }

    @GetMapping("/test3")
    public Object test3(@RequestParam Boolean desensitized) {
        return testService.getList1(desensitized);
    }

    @GetMapping("/test4")
    public BaseResponse<List<Test>> test4(@RequestParam Boolean desensitized) {
        return testService.getList2(desensitized);
    }

    @GetMapping("/test5")
    public BaseResponse<List<Test>> test5(@RequestParam Boolean desensitized) {
        return testService.getList3(desensitized);
    }

}
