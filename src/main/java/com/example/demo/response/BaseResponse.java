package com.example.demo.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: EDY
 * @Date: 2023/11/9 17:09
 * @Version: v1.0.0
 * @Description: TODO
 **/
@Data
@AllArgsConstructor
public class BaseResponse<T> implements Serializable {

    private static final long serialVersionUID = -620727297442556977L;

    private String code;

    private String description;

    private T data;

    @JsonIgnore
    public Boolean ok() {
        return true;
    }

    public BaseResponse() {
        this.code = "000000";
        this.description = "success";
    }

    public BaseResponse(T data) {
        this.code = "000000";
        this.description = "success";
        this.data = data;
    }

    public BaseResponse(String code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public String toString() {

        return "BaseResponse{" + "code='" + code + '\'' + ", description='" + description + '\'' + ", data=" + data
                + '}';
    }
}
