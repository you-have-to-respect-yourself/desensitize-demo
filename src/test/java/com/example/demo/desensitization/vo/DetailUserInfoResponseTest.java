package com.example.demo.desensitization.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by YangYuanDong on 2019/5/29.
 */
@Setter
@Getter

public class DetailUserInfoResponseTest implements Serializable {
    private static final long serialVersionUID = 1999052150548658807L;
    private Long userId;
    private String tagInfoList;

    private String mobile;

    private String brand;

    private String userName;

    private String nickName;

    private String idpId;

    private Date createTime;

    private Date updateTime;

    private String email;

    private String avatar;

    private String profession;
    private String annualIncome;
    private String qqNum;
    private String weChat;
    private String microblog;
    private String autoHome;
    private String address;
}
