package com.example.demo.desensitization.util;

import com.example.demo.desensitization.vo.DetailUserInfoResponseTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class DesensitizedUtilTest {


    @InjectMocks
    DesensitizedUtil util;

    @Test
    public void test() {
        List testData = new ArrayList() {{
            add(new DetailUserInfoResponseTest() {{
                setMobile("13383889999");
                setBrand("test");
                setWeChat("410302199987895418");
            }});
        }};
        DesensitizedUtil.desensitizeString(0, 0, 5, "123");
        DesensitizedUtil.desensitizeStringList(0, 0, -1, new ArrayList() {{
            add("123");
        }});
        DesensitizedUtil.desensitizeStringList(0, 0, -1, new ArrayList() {{
        }});


        DesensitizedUtil.desensitizeListEntity(0, 0, -1, null, "");
        DesensitizedUtil.desensitizeListEntity(0, 0, -1, new ArrayList() {{
            add(null);
        }}, "");
        DesensitizedUtil.desensitizeListEntity(0, 0, null, "", "");
        DesensitizedUtil.desensitizeListEntity(0, 0, new ArrayList() {{
            add(null);
        }}, "", "");

        DesensitizedUtil.desensitizeListEntity(0, 0, testData, "mobile", "brand", "email");

        DesensitizedUtil.desensitizePhone("13383889999");

        DesensitizedUtil.desensitizePhoneList(testData, "mobile");

        DesensitizedUtil.desensitizeMobile("13383889999");

        DesensitizedUtil.desensitizeMobileList(testData, "mobile");

        DesensitizedUtil.desensitizeIdCard("410302199987895418");

        DesensitizedUtil.desensitizeIdCardList(testData, "weChat");

        DesensitizedUtil.desensitizeIdPassport("123456789");

        DesensitizedUtil.desensitizeIdPassportList(testData, "weChat");

        DesensitizedUtil.desensitizeSoldier("123456789");

        DesensitizedUtil.desensitizeSoldierList(testData, "weChat");

        DesensitizedUtil.desensitizePolice("123456789");

        DesensitizedUtil.desensitizePoliceList(testData, "weChat");
    }


}
