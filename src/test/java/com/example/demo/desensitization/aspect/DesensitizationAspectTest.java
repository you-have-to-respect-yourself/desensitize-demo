package com.example.demo.desensitization.aspect;

import com.example.demo.desensitization.annotation.Desensitization;
import com.example.demo.desensitization.annotation.DesensitizationFiled;
import com.example.demo.desensitization.enums.DesensitizationFiledEnum;
import com.example.demo.desensitization.vo.DetailUserInfoResponseTest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DesensitizationAspectTest {

    @InjectMocks
    DesensitizationAspect aspect;

    @Mock
    ProceedingJoinPoint pjp;

    @Test
    public void serviceAspectTest() {
        aspect.serviceAspect();
    }

    @Test
    public void around1() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("desensitized", String.valueOf(Boolean.TRUE.booleanValue()));
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        MethodSignature signature = mock(MethodSignature.class);
        when(pjp.getSignature()).thenReturn(signature);

        try {
            when(pjp.proceed()).thenReturn(someMethod1());
            when(signature.getMethod()).thenReturn(this.getClass().getDeclaredMethod("someMethod1"));
            aspect.around(pjp);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Test
    public void around2() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("desensitized", String.valueOf(Boolean.TRUE.booleanValue()));
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        MethodSignature signature = mock(MethodSignature.class);
        when(pjp.getSignature()).thenReturn(signature);

        try {
            when(signature.getMethod()).thenReturn(this.getClass().getDeclaredMethod("someMethod1"));
            aspect.around(pjp);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Test
    public void around3() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("desensitized", String.valueOf(Boolean.TRUE.booleanValue()));
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        MethodSignature signature = mock(MethodSignature.class);
        when(pjp.getSignature()).thenReturn(signature);

        try {
            when(pjp.proceed()).thenReturn(someMethod2());
            when(signature.getMethod()).thenReturn(this.getClass().getDeclaredMethod("someMethod2"));
            aspect.around(pjp);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Test
    public void around4() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("desensitized", String.valueOf(Boolean.TRUE.booleanValue()));
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        MethodSignature signature = mock(MethodSignature.class);
        when(pjp.getSignature()).thenReturn(signature);

        try {
            when(pjp.proceed()).thenReturn(someMethod3());
            when(signature.getMethod()).thenReturn(this.getClass().getDeclaredMethod("someMethod3"));
            aspect.around(pjp);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Test
    public void around5() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("desensitized", String.valueOf(Boolean.FALSE.booleanValue()));
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        MethodSignature signature = mock(MethodSignature.class);
        when(pjp.getSignature()).thenReturn(signature);

        try {
            when(pjp.proceed()).thenReturn(someMethod1());
            when(signature.getMethod()).thenReturn(this.getClass().getDeclaredMethod("someMethod1"));
            aspect.around(pjp);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Test
    public void around6() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("desensitized", String.valueOf(Boolean.TRUE.booleanValue()));
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        MethodSignature signature = mock(MethodSignature.class);
        when(pjp.getSignature()).thenReturn(signature);

        try {
            when(pjp.proceed()).thenReturn(someMethod4());
            when(signature.getMethod()).thenReturn(this.getClass().getDeclaredMethod("someMethod4"));
            aspect.around(pjp);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }


    @Test
    public void around7() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("desensitized", String.valueOf(Boolean.TRUE.booleanValue()));
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        MethodSignature signature = mock(MethodSignature.class);

        when(pjp.getSignature()).thenReturn(signature);

        try {
            when(pjp.proceed()).thenReturn(someMethod5());
            when(signature.getMethod()).thenReturn(this.getClass().getDeclaredMethod("someMethod5"));
            aspect.around(pjp);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

    }


    @Desensitization({
            @DesensitizationFiled(value = "mobile", deFiledEnum = DesensitizationFiledEnum.PHONE),
    })
    public DetailUserInfoResponseTest someMethod1() {
        DetailUserInfoResponseTest result = new DetailUserInfoResponseTest();
        result.setMobile("13383889999");
        return result;
    }


    @Desensitization({
            @DesensitizationFiled(value = "mobile", deFiledEnum = DesensitizationFiledEnum.PHONE),
    })
    public List<DetailUserInfoResponseTest> someMethod2() {
        DetailUserInfoResponseTest result = new DetailUserInfoResponseTest();
        result.setMobile("13383889999");
        return new ArrayList() {{
            add(result);
        }};
    }

    @Desensitization({
            @DesensitizationFiled(value = "mobile1", deFiledEnum = DesensitizationFiledEnum.PHONE),
    })
    public List<DetailUserInfoResponseTest> someMethod3() {
        DetailUserInfoResponseTest result = new DetailUserInfoResponseTest();
        return new ArrayList() {{
            add(result);
        }};
    }

    @Desensitization({
            @DesensitizationFiled(value = "mobile", deFiledEnum = DesensitizationFiledEnum.PHONE),
    })
    public List<DetailUserInfoResponseTest> someMethod4() {
        return new ArrayList() {{
            add(null);
        }};
    }

    @Desensitization({
            @DesensitizationFiled(value = "mobile", deFiledEnum = DesensitizationFiledEnum.PHONE),
            @DesensitizationFiled(value = "address", deFiledEnum = DesensitizationFiledEnum.ADDRESS),
            @DesensitizationFiled(value = "email", deFiledEnum = DesensitizationFiledEnum.EMAIL),
            @DesensitizationFiled(value = "nickName", deFiledEnum = DesensitizationFiledEnum.NICK_NAME),
            @DesensitizationFiled(value = "brand", deFiledEnum = DesensitizationFiledEnum.NULL),
    })
    public List<DetailUserInfoResponseTest> someMethod5() {
        DetailUserInfoResponseTest result = new DetailUserInfoResponseTest();
        result.setMobile("13383889999");
        result.setAddress("上海市嘉定区于田路7号");
        result.setEmail("999999@saic-vw.com");
        result.setNickName("1");

        DetailUserInfoResponseTest result1 = new DetailUserInfoResponseTest();
        result1.setNickName("12");
        DetailUserInfoResponseTest result2 = new DetailUserInfoResponseTest();
        result2.setNickName("123");
        DetailUserInfoResponseTest result3 = new DetailUserInfoResponseTest();
        result3.setNickName("124");

        DetailUserInfoResponseTest result4 = new DetailUserInfoResponseTest();
        result3.setBrand("test");
        return new ArrayList() {{
            add(result);
            add(result1);
            add(result2);
            add(result3);
            add(result4);

        }};
    }
}
